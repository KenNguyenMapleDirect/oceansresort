<?php

namespace App\Http\Controllers\Voyager;

use App\AugCore;
use App\SepCore;
use App\OctCore;
use App\Data;
use App\JulyCore;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Intervention\Image\Constraint;
use Intervention\Image\Facades\Image;
use League\Flysystem\Util;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Http\Controllers\Controller;

class VoyagerController
{
    public function index(Request $request)
    {
        //define data return
        $data = new \stdClass();
        $data->first_name = '';
        $data->last_name = '';
        $data->BAC_Tier = '';
        $data->BAC_Reward_Points = 0;
        $data->BAC_Tier_Points = 0;
        $data->BAC_Points_Next_Tier = 0;
        $data->updated_at = now();
        //Flag to check if account exist in database
        $data->flgAccount = 0;
        if($request->AN)
        {
            $accountId = base64_decode($request->AN);
            //Get data from Datas table
            $datas = Data::where('BAC_Account_Combined', $accountId)->first();
            //get user data
            if($datas)
            {
                $data->first_name = $datas->BAC_FName;
                $data->last_name = $datas->BAC_Lname;
                $data->BAC_Tier = $datas->BAC_Tier;
                $data->BAC_Reward_Points = $datas->BAC_Reward_Points;
                $data->BAC_Tier_Points = $datas->BAC_Tier_Points;
                $data->BAC_Points_Next_Tier = $datas->BAC_Points_Next_Tier;
            }

            //Get data from Sep2021 table
            //Define data type flag
            $data->sepCoreSM = 0;
            $data->sepCorePC = 0;
            $data->sepReminderPC = 0;
            $data->sepFlatScreenPC = 0;
            $data->sepWeekendReminderPC = 0;
            $data->sepWeekendReminderPC2 = 0;

            $dataFromSep2021 = SepCore::where('BAC_Account',$data->BAC_Account_Combined)->get();
            if($dataFromSep2021)
            {
                foreach($dataFromSep2021 as $singleDataFromSep2021)
                {
                    if($singleDataFromSep2021->BAC_Mailer_Type === "Core SM"){
                        $data->sepCoreSM = 0;
                        $data->flgSM =1;
                        $data->sepCoreSMResult1 = $singleDataFromSep2021->BAC_Img_Page01.".jpg";
                        $data->sepCoreSMResult2 = $singleDataFromSep2021->BAC_Img_Page02.".jpg";
                        $data->sepCoreSMResult3 = $singleDataFromSep2021->BAC_Img_Page03.".jpg";
                        $data->sepCoreSMResult4 = $singleDataFromSep2021->BAC_Img_Page04.".jpg";
                        $data->sepCoreSMResult5 = $singleDataFromSep2021->BAC_Img_Page05.".jpg";
                        $data->sepCoreSMResult6 = $singleDataFromSep2021->BAC_Img_Page06.".jpg";
                    }

                    if($singleDataFromSep2021->BAC_Mailer_Type === "Core PC"){
                        $data->sepCorePC = 0;
                        $data->flgPC =1;
                        $data->sepCorePCResult1 = $singleDataFromSep2021->BAC_Img_Page01.".jpg";
                        $data->sepCorePCResult2 = $singleDataFromSep2021->BAC_Img_Page02.".jpg";
                        $data->sepCorePCResult3 = $singleDataFromSep2021->BAC_Img_Page03.".jpg";
                        $data->sepCorePCResult4 = $singleDataFromSep2021->BAC_Img_Page04.".jpg";
                    }

                    if($singleDataFromSep2021->BAC_Mailer_Type === "FLAT_SCREEN_PC"){
                        $data->sepFlatScreenPC = 0;
                        $data->flgPC =1;
                        $data->sepFlatScreenPCResult1 = $singleDataFromSep2021->BAC_Img_Page01.".jpg";
                        $data->sepFlatScreenPCResult2 = $singleDataFromSep2021->BAC_Img_Page02.".jpg";
                    }
                    if($singleDataFromSep2021->BAC_Mailer_Type === "WEEKEND_REMINDER_0919"){
                        $data->sepWeekendReminderPC = 0;
                        $data->flgPC =1;
                        $data->sepWeekendReminderPCResult1 = $singleDataFromSep2021->BAC_Img_Page01.".jpg";
                        $data->sepWeekendReminderPCResult2 = $singleDataFromSep2021->BAC_Img_Page02.".jpg";
                    }
                    if($singleDataFromSep2021->BAC_Mailer_Type === "WEEKEND_REMINDER_0926"){
                        $data->sepWeekendReminderPC2 = 0;
                        $data->flgPC =1;
                        $data->sepWeekendReminder2PCResult1 = $singleDataFromSep2021->BAC_Img_Page01.".jpg";
                        $data->sepWeekendReminder2PCResult2 = $singleDataFromSep2021->BAC_Img_Page02.".jpg";
                    }



                }
            }

            //Get data from Oct2021 table
            //Define data type flag
            $data->octNmPC = 0;
            $data->octNmPCLabel = '';
            $data->oct100RewardsPC = 0;
            $data->oct100RewardsPCLabel = '';
            $data->octCoreSM1 = 0;
            $data->octCoreSM1Label = '';
            $data->octCoreSM2 = 0;
            $data->octCoreSM2Label = '';
            $data->octJackportSweeps = 0;
            $data->octJackportSweepsLabel = '';
            $data->octWeekendReminderPC = 0;
            $data->octNewMemberPC = 0;
            $data->octHighEndPC = 0;
            $data->octWeekendReminder1017PC = 0;
            $data->octNewMember1212PC = 0;
            $data->octMIMIPC = 0;


            $dataFromOct2021 = OctCore::where('BAC_Account',$data->BAC_Account_Combined)->get();
            if($dataFromOct2021)
            {
                foreach($dataFromOct2021 as $singleDataFromOct2021)
                {

                    if($singleDataFromOct2021->BAC_Mailer_Type === "NEW_MEMBER_0921"){
                        $data->octNmPC = 1;
                        $data->octNmPCLabel = $singleDataFromOct2021->BAC_Label;
                        $data->flgPC =1;
                        $data->octNmPCResult1 = $singleDataFromOct2021->BAC_Img_Page01.".jpg";
                        $data->octNmPCResult2 = $singleDataFromOct2021->BAC_Img_Page02.".jpg";
                    }

                    if($singleDataFromOct2021->BAC_Mailer_Type === "JACKPOT_SWEEPS"){
                        $data->octJackportSweeps = 0;
                        $data->octJackportSweepsLabel = $singleDataFromOct2021->BAC_Label;
                        $data->flgPC =1;
                        $data->octJackportSweepsResult1 = $singleDataFromOct2021->BAC_Img_Page01.".jpg";
                        $data->octJackportSweepsResult2 = $singleDataFromOct2021->BAC_Img_Page02.".jpg";
                    }

                    if($singleDataFromOct2021->BAC_Mailer_Type === "100X_REWARDS"){
                        $data->oct100RewardsPC = 1;
                        $data->oct100RewardsPCLabel = $singleDataFromOct2021->BAC_Label;
                        $data->flgPC =1;
                        $data->oct100RewardsPCResult1 = $singleDataFromOct2021->BAC_Img_Page01.".jpg";
                        $data->oct100RewardsPCResult2 = $singleDataFromOct2021->BAC_Img_Page02.".jpg";
                    }
                    if($singleDataFromOct2021->BAC_Mailer_Type === "WEEKEND_REMINDER_1017"){
                        $data->octWeekendReminderPC = 0;
                        $data->WeekendReminderPCLabel = $singleDataFromOct2021->BAC_Label;
                        $data->flgPC =1;
                        $data->octWeekendReminderPCResult1 = $singleDataFromOct2021->BAC_Img_Page01.".jpg";
                        $data->octWeekendReminderPCResult2 = $singleDataFromOct2021->BAC_Img_Page02.".jpg";
                    }
                    if($singleDataFromOct2021->BAC_Mailer_Type === "NEW_MEMBER_1026"){
                        $data->octNewMemberPC = 1;
                        $data->NewMemberPCLabel = $singleDataFromOct2021->BAC_Label;
                        $data->flgPC =1;
                        $data->octNewMemberPCResult1 = $singleDataFromOct2021->BAC_Img_Page01.".jpg";
                        $data->octNewMemberPCResult2 = $singleDataFromOct2021->BAC_Img_Page02.".jpg";
                    }
                    if($singleDataFromOct2021->BAC_Mailer_Type === "High End PC"){
                        $data->octHighEndPC = 1;
                        $data->HighEndrPCLabel = $singleDataFromOct2021->BAC_Label;
                        $data->flgPC =1;
                        $data->octHighEndPCResult1 = $singleDataFromOct2021->BAC_Img_Page01.".jpg";
                        $data->octHighEndPCResult2 = $singleDataFromOct2021->BAC_Img_Page02.".jpg";
                    }
                    if($singleDataFromOct2021->BAC_Mailer_Type === "MIMI_CHOO_PC"){
                        $data->octMIMIPC = 1;
                        $data->OctMIMIPCLabel = $singleDataFromOct2021->BAC_Label;
                        $data->flgPC =1;

                    }
                    if($singleDataFromOct2021->BAC_Mailer_Type === "Core SM1"){
                        $data->octCoreSM1 = 1;
                        $data->octCoreSM1Label = $singleDataFromOct2021->BAC_Label;
                        $data->flgSM =1;
                        $data->octCoreSM1Result1 = $singleDataFromOct2021->BAC_Img_Page01.".jpg";
                        $data->octCoreSM1Result2 = $singleDataFromOct2021->BAC_Img_Page02.".jpg";
                        $data->octCoreSM1Result3 = $singleDataFromOct2021->BAC_Img_Page03.".jpg";
                        $data->octCoreSM1Result4 = $singleDataFromOct2021->BAC_Img_Page04.".jpg";
                        $data->octCoreSM1Result5 = $singleDataFromOct2021->BAC_Img_Page05.".jpg";
                        $data->octCoreSM1Result6 = $singleDataFromOct2021->BAC_Img_Page06.".jpg";
                        $data->octCoreSM1Result7 = $singleDataFromOct2021->BAC_Img_Page07.".jpg";
                        $data->octCoreSM1Result8 = $singleDataFromOct2021->BAC_Img_Page08.".jpg";
                    }
                    if($singleDataFromOct2021->BAC_Mailer_Type === "Core SM2"){
                        $data->octCoreSM2 = 1;
                        $data->octCoreSM2Label = $singleDataFromOct2021->BAC_Label;
                        $data->flgSM =1;
                        $data->octCoreSM2Result1 = $singleDataFromOct2021->BAC_Img_Page01.".jpg";
                        $data->octCoreSM2Result2 = $singleDataFromOct2021->BAC_Img_Page02.".jpg";
                        $data->octCoreSM2Result3 = $singleDataFromOct2021->BAC_Img_Page03.".jpg";
                        $data->octCoreSM2Result4 = $singleDataFromOct2021->BAC_Img_Page04.".jpg";

                    }
                    if($singleDataFromOct2021->BAC_Mailer_Type === "WEEKEND_REMINDER_1017"){
                        $data->octWeekendReminder1017PC = 0;
                        $data->WeekendReminder1017PCLabel = $singleDataFromOct2021->BAC_Label;
                        $data->flgPC =1;
                        $data->octWeekendReminder1017PCResult1 = $singleDataFromOct2021->BAC_Img_Page01.".jpg";
                        $data->octWeekendReminder1017PCResult2 = $singleDataFromOct2021->BAC_Img_Page02.".jpg";
                    }
                    if($singleDataFromOct2021->BAC_Mailer_Type === "NEW_MEMBER_1212"){
                        $data->octNewMember1212PC = 1;
                        $data->octNewMember1212PCLabel = $singleDataFromOct2021->BAC_Label;
                        $data->flgPC =1;
                        $data->octNewMember1212PCResult1 = $singleDataFromOct2021->BAC_Img_Page01.".jpg";
                        $data->octNewMember1212PCResult2 = $singleDataFromOct2021->BAC_Img_Page02.".jpg";
                    }

                }
            }
        }
        return view('player-dashboard')->with('data', $data);
//        }
    }

    public function makeImage($accountId, $imageTextData)
    {
        $img = Image::make('https://s3.us-east-2.amazonaws.com/ballysac.maplewebservices.com/september_2021/hi_res/131941_SEPT2021_350K_REMINDER_P01.jpg');
        $img->rotate(-90);
        $img->text($imageTextData, 140, 1590, function ($font) {
            $font->file(public_path('PostCardImage/350ReminderPostCard/fonts/National2Condensed-Extrabold.otf'));
            $font->size(170);
            $font->color('#01b8de');
            $font->align('left');
            $font->angle(0);
        });
//            if (!File::exists('PostCardImage/350ReminderPostCard/ImageByAccountId/' . $accountId . '.jpg'))
            {
                $img->save(public_path('PostCardImage/350ReminderPostCard/ImageByAccountId/' . $accountId . '.jpg'));
            }
    }

}
