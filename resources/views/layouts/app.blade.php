<!DOCTYPE html>
<html><head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" type="text/css" href="/HeaderFooterAssets/mos-ORC.css">

    <link rel="stylesheet" type="text/css" href="/HeaderFooterAssets/css.css" media="screen">

    <link rel="stylesheet" id="wc-shortcodes-style-css" href="/HeaderFooterAssets/style.css" type="text/css" media="all">
    <link rel="stylesheet" id="genericons-css" href="/HeaderFooterAssets/genericons.css" type="text/css" media="all">

    <link rel="stylesheet" id="dad-style-css" href="/HeaderFooterAssets/framestyle.css" type="text/css" media="all">

    <script type="text/javascript" async="" src="/HeaderFooterAssets/fbevents.js"></script><script type="text/javascript" async="" src="/HeaderFooterAssets/analytics.js"></script><script async="" src="/HeaderFooterAssets/gtm.js"></script><script type="text/javascript" src="/HeaderFooterAssets/jquery_004.js"></script>
    <script type="text/javascript" src="/HeaderFooterAssets/core.js"></script>
    <script type="text/javascript" src="/HeaderFooterAssets/jquery_003.js"></script>

    <link rel="icon" href="https://theoceanac.com/wp-content/uploads/2019/04/cropped-siteicon-32x32.png" sizes="32x32">
    <link rel="icon" href="https://theoceanac.com/wp-content/uploads/2019/04/cropped-siteicon-192x192.png" sizes="192x192">
    <link rel="apple-touch-icon-precomposed" href="https://theoceanac.com/wp-content/uploads/2019/04/cropped-siteicon-180x180.png">
    <meta name="msapplication-TileImage" content="https://theoceanac.com/wp-content/uploads/2019/04/cropped-siteicon-270x270.png">

    <title>Ocean Casino Resort | Atlantic City</title>

    <!-- Google Tag Manager - DAD - 20190520 -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-T4BH3TS');</script>
    <!-- End Google Tag Manager -->

    <meta http-equiv="origin-trial" content="A7pIQvxLYo3CR+raAuUI8Rfr81qhL4u1c7JIHkL6Clt5Jq0Xi/EH8+lTm0ZzKyi+618U2BxGlpKmCTclb793+AUAAACKeyJvcmlnaW4iOiJodHRwczovL2dvb2dsZXRhZ21hbmFnZXIuY29tOjQ0MyIsImZlYXR1cmUiOiJDb252ZXJzaW9uTWVhc3VyZW1lbnQiLCJleHBpcnkiOjE2MzQwODMxOTksImlzU3ViZG9tYWluIjp0cnVlLCJpc1RoaXJkUGFydHkiOnRydWV9"></head>

<body class="page-template-default page page-id-50 custom-background wp-custom-logo wp-embed-responsive  wc-shortcodes-font-awesome-enabled custom-background-image no-sidebar">

<!-- Google Tag Manager (noscript) - DAD - 20190520 -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-T4BH3TS" height="0" width="0" style="display:none;visibility:hidden">
    </iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<iframe style="display: none; visibility: hidden;" src="/HeaderFooterAssets/activityi.html" width="0" height="0"></iframe><div id="page" class="site">
    <div class="site-inner">

        <header id="masthead" class="site-header" role="banner">
            <div class="site-header-main">
                <div class="site-branding">
                    <a href="https://theoceanac.com/" class="custom-logo-link" rel="home" itemprop="url"><img src="/HeaderFooterAssets/cropped-ocean-casino-resort.png" class="custom-logo" alt="cropped-ocean-casino-resort" itemprop="logo" width="200" height="111"></a>
                    <p class="site-title"><a href="https://theoceanac.com/" rel="home">Ocean Casino Resort</a>
                    </p>
                </div>
                <!-- .site-branding -->

                <button id="menu-toggle" class="menu-toggle">Menu</button>

                <div id="site-header-menu" class="site-header-menu">
                    <nav id="site-navigation" class="main-navigation" role="navigation" aria-label="Primary Menu">
                        <div class="menu-main-menu-container">
                            <ul id="menu-main-menu" class="primary-menu">
                                <li id="menu-item-30" class="menu-item menu-item-type-post_type menu-item-object-page  menu-item-30"><a href="https://theoceanac.com/hotel/">Hotel + Spa</a>

                                </li>
                                <li id="menu-item-29" class="menu-item menu-item-type-post_type menu-item-object-page  menu-item-29"><a href="https://theoceanac.com/casino/">Casino + Sports</a>

                                </li>
                                <li id="menu-item-28" class="menu-item menu-item-type-post_type menu-item-object-page  menu-item-28"><a href="https://theoceanac.com/restaurants/">Dining</a>

                                </li>
                                <li id="menu-item-603" class="menu-item menu-item-type-custom menu-item-object-custom  menu-item-603"><a href="https://theoceanac.com/events/">Entertainment + Events</a>

                                </li>
                                <li id="menu-item-26" class="menu-item menu-item-type-post_type menu-item-object-page  menu-item-26"><a href="https://theoceanac.com/nightlife/">Nightlife</a>

                                </li>
                                <li id="menu-item-23" class="menu-item menu-item-type-post_type menu-item-object-page  menu-item-23"><a href="https://theoceanac.com/meetings/">Meetings</a>
                                </li>

                                <li id="menu-item-685" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-685"><a href="https://www.theoceanac.com/photo-gallery/">Photos + Videos</a></li>
                            </ul>
                        </div>
                    </nav>
                    <!-- .main-navigation -->

                </div>
                <!-- .site-header-menu -->
            </div>
            <!-- .site-header-main -->

            <br>
            <div id="booking">
                <div class="wc-shortcodes-row wc-shortcodes-item wc-shortcodes-clearfix">


                    <div class="wc-shortcodes-column wc-shortcodes-content wc-shortcodes-one-fourth wc-shortcodes-column-first login">
                        <a href="https://www.theoceanac.com/rewardslogin/auth/offers.php"><img src="/HeaderFooterAssets/user.png" alt="account login"><p>BACK TO PORTAL</p></a>
                    </div>

                    <div class="wc-shortcodes-column wc-shortcodes-content wc-shortcodes-one-fourth wc-shortcodes-column submit">

                        <div class="bookingbtn"><a href="https://theoceanac.windsurfercrs.com/ibe/index.aspx?propertyID=15345"><img src="/HeaderFooterAssets/bell.png" alt="book a room"><p>BOOK A ROOM</p></a></div>

                    </div>




                    <div class="wc-shortcodes-column wc-shortcodes-content wc-shortcodes-one-fourth wc-shortcodes-column online">
                        <a href="https://www.oceanonlinecasino.com/"><img src="/HeaderFooterAssets/chip.png" alt="play online"><p>ONLINE CASINO</p></a>
                    </div>

                    <div class="wc-shortcodes-column wc-shortcodes-content wc-shortcodes-one-fourth wc-shortcodes-column-last sports">
                        <a href="https://us.williamhill.com/nj/campaigns/ocean-casino/_recache"><img src="/HeaderFooterAssets/ball.png" alt="play online"><p>ONLINE SPORTS</p></a>
                    </div>


                </div>
            </div>
            <img src="/HeaderFooterAssets/atlantic-city-slots_0003_20190509_Casino4.jpg" alt="Ocean Casino Resort Atlantic City">

            <header class="entry-header greenblock">
                <h1 class="entry-title">FUTURE OFFERS</h1>
            </header>

        </header><!-- .site-header -->
        <div class="loader"></div>
        <div id="content" class="site-content">
                @yield('content')
        </div><!-- .site-content -->

        <aside class="fatfooter" role="complementary">
            <div class="first quarter left widget-area">
                <div id="text-2" class="widget-container widget_text">
                    <div class="textwidget"><h4>RESERVATIONS</h4>
                        <p><a href="tel:1-609-783-8000">609-783-8000</a></p>
                        <h4>OCEAN CASINO RESORT</h4>
                        <p>500 Boardwalk<br>
                            Atlantic City, NJ 08401</p>
                    </div>
                </div>
            </div><!-- .first .widget-area -->

            <div class="second quarter widget-area">
                <div id="nav_menu-2" class="widget-container widget_nav_menu"><h3 class="widget-title">About Ocean</h3>
                    <div class="menu-footer-about-container">
                        <ul id="menu-footer-about" class="menu">
                            <li id="menu-item-56" class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-50 current_page_item menu-item-56"><a href="https://theoceanac.com/contact-us/" aria-current="page">Contact Us</a></li>
                            <li id="menu-item-681" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-681"><a href="https://careerslogin.theoceanac.com/?_ga=2.181271015.1114395623.1554989915-536327782.1554989915">Careers</a></li>
                            <li id="menu-item-679" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-679"><a href="https://theoceanac.com/terms-of-service/">Terms of Service</a></li>
                            <li id="menu-item-680" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-680"><a href="https://theoceanac.com/privacy-policy/">Privacy Policy</a></li>
                        </ul>
                    </div>
                </div>
            </div><!-- .second .widget-area -->

            <div class="third quarter widget-area">
                <div id="nav_menu-3" class="widget-container widget_nav_menu"><h3 class="widget-title">Resources</h3>
                    <div class="menu-footer-resources-container">
                        <ul id="menu-footer-resources" class="menu">
                            <li id="menu-item-682" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-682"><a href="https://theoceanac.com/in-the-news/">In The News</a></li>
                            <li id="menu-item-683" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-683"><a href="https://theoceanac.com/press-releases/">Press Releases</a></li>
                            <li id="menu-item-684" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-684"><a href="https://theoceanac.com/frequently-asked-questions/">Frequently Asked Questions</a></li>
                            <li id="menu-item-685" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-685"><a href="https://www.chargerback.com/ReportLostItemCBEmbed.asp?CustomerID=16367">Lost &amp; Found</a></li>
                            <li id="menu-item-685" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-685"><a href="https://www.theoceanac.com/photo-gallery/">Photo Gallery</a></li>
                        </ul>
                    </div>
                </div>
            </div><!-- .third .widget-area -->

            <div class="fourth quarter right widget-area">
                <div id="nav_menu-4" class="widget-container widget_nav_menu"><h3 class="widget-title">Getting Here</h3>
                    <div class="menu-footer-getting-here-container">
                        <ul id="menu-footer-getting-here" class="menu">
                            <li id="menu-item-692" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-692"><a href="https://theoceanac.com/parking/">Parking</a></li>
                            <li id="menu-item-693" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-693"><a href="https://theoceanac.com/bus-tours/">Bus Tours</a></li>
                            <li id="menu-item-694" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-694"><a href="https://theoceanac.com/directions/">Directions</a></li>
                            <li id="menu-item-695" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-695"><a href="https://theoceanac.com/property-map/">Property Map</a></li>
                        </ul>
                    </div>
                </div>
            </div><!-- .fourth .widget-area -->

        </aside><!-- #fatfooter -->

        <footer id="colophon" class="site-footer" role="contentinfo">

            <div class="ftr-extras">

                <div class="wc-shortcodes-row wc-shortcodes-item wc-shortcodes-clearfix">
                    <div class="wc-shortcodes-column wc-shortcodes-content wc-shortcodes-one-half wc-shortcodes-column-first ">
                        Gambling Problem? Call 1-800-GAMBLER | Must be 21.
                    </div>

                    <div class="wc-shortcodes-column wc-shortcodes-content wc-shortcodes-one-half wc-shortcodes-column-last">
                        <a href="https://theoceanac.mymapleonline.com/responsible-gaming/">Responsible Gaming</a> | <a href="https://theoceanac.com/accessibility/">Accessibility</a>
                        <a href="https://www.facebook.com/theoceanac/"><img src="/HeaderFooterAssets/facebook.png" width="38"></a>
                        <a href="https://twitter.com/TheOceanAC"><img src="/HeaderFooterAssets/twitter.png" width="38"></a>
                        <a href="https://www.instagram.com/theoceanac/?hl=en"><img src="/HeaderFooterAssets/instagram.png" width="38"></a>
                    </div>
                </div>

            </div>

            <div class="site-info">
                © 2019
                <span class="site-title"><a href="https://theoceanac.com/" rel="home">Ocean Casino Resort</a>.</span>

                All Rights Reserved.

            </div><!-- .site-info -->
        </footer><!-- .site-footer -->
    </div><!-- .site-inner -->
</div><script type="text/javascript" id="" src="/HeaderFooterAssets/up.js"></script><iframe style="width: 0px; height: 0px; border: 0px none; display: none !important;" src="javascript:false"></iframe>
<script type="text/javascript" id="">cntrUpTag.track("cntrData","c4f43836cfc4097b");</script>
<div id="lightboxOverlay" class="lightboxOverlay" style="display: none;"></div><div id="lightbox" class="lightbox" style="display: none;"><div class="lb-dataContainer"><div class="lb-data">	<div class="lb-details"><span class="lb-caption"></span><span class="lb-number"></span></div><div class="lb-closeContainer"><a class="lb-close"></a></div></div></div><div class="lb-outerContainer"><div class="lb-container"><img class="lb-image" src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw=="><div class="lb-nav"><a class="lb-prev" href=""></a><a class="lb-next" href=""></a></div><div class="lb-loader"><a class="lb-cancel"></a></div></div></div></div><!-- .site -->

<script type="text/javascript" src="/HeaderFooterAssets/skip-link-focus-fix.js"></script>
<script type="text/javascript">
    /* <![CDATA[ */
    var screenReaderText = {"expand":"expand child menu","collapse":"collapse child menu"};
    /* ]]> */
</script>

<script type="text/javascript" src="/HeaderFooterAssets/functions.js"></script>



</body></html>
